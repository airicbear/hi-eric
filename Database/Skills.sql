-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 21, 2019 at 06:27 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id4080927_databasetest`
--

-- --------------------------------------------------------

--
-- Table structure for table `Skills`
--

CREATE TABLE `Skills` (
  `ID` smallint(6) NOT NULL,
  `Category` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `Name` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `Experience` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `URL` varchar(2083) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Skills`
--

INSERT INTO `Skills` (`ID`, `Category`, `Name`, `Experience`, `URL`) VALUES
(1, 'Languages/Frameworks', 'HTML', 'primary', 'https://www.w3schools.com/html/'),
(2, 'Languages/Frameworks', 'CSS', 'primary', 'https://www.w3schools.com/css/'),
(3, 'Languages/Frameworks', 'Javascript', 'primary', 'https://www.w3schools.com/js/'),
(4, 'Languages/Frameworks', 'SQL', 'primary', 'https://www.w3schools.com/sql/'),
(5, 'Languages/Frameworks', 'PHP', 'primary', 'https://www.w3schools.com/php/'),
(6, 'Languages/Frameworks', 'Bootstrap 4', 'primary', 'https://www.w3schools.com/bootstrap4/'),
(7, 'Languages/Frameworks', 'Java', 'primary', 'https://en.wikipedia.org/wiki/Java_(programming_language)'),
(8, 'Languages/Frameworks', 'C# (Unity)', 'primary', 'https://docs.microsoft.com/en-us/dotnet/csharp/'),
(9, 'Software', 'Blender', 'primary', 'https://www.blender.org/'),
(10, 'Software', 'Gimp', 'primary', 'https://www.gimp.org/'),
(11, 'Software', 'Unity', 'primary', 'https://unity3d.com/'),
(12, 'Software', 'VSCode', 'primary', 'https://code.visualstudio.com/'),
(13, 'Software', 'FileZilla', 'primary', 'https://filezilla-project.org/'),
(14, 'Software', 'Eclipse', 'primary', 'https://www.eclipse.org/'),
(15, 'Software', 'Alice', 'primary', 'https://www.alice.org/'),
(16, 'Software', 'Cinema4D', 'secondary', 'https://www.maxon.net/en/products/cinema-4d/overview/'),
(17, 'Software', 'IntelliJ', 'secondary', 'https://www.jetbrains.com/idea/'),
(18, 'Software', 'Logic Pro X', 'primary', 'https://www.apple.com/logic-pro/'),
(20, 'General', 'Dance', 'primary', 'https://en.wikipedia.org/wiki/Breakdancing'),
(21, 'General', 'Web Dev', 'primary', 'https://en.wikipedia.org/wiki/Web_development'),
(22, 'General', 'Game Dev', 'primary', 'https://en.wikipedia.org/wiki/Video_game_development'),
(23, 'Languages/Frameworks', 'Python', 'primary', 'https://www.python.org/'),
(24, 'Software', 'Processing', 'primary', 'https://processing.org/'),
(19, 'Software', 'Adobe Illustrator', 'secondary', 'https://www.adobe.com/products/illustrator.html'),
(25, 'Languages/Frameworks', 'Heroku', 'primary', 'https://www.heroku.com/'),
(26, 'Languages/Frameworks', 'Git', 'primary', 'https://git-scm.com/'),
(27, 'Software', 'GitHub', 'primary', 'https://github.com/'),
(28, 'Software', 'Vectorworks', 'Primary', 'http://www.vectorworks.net/'),
(29, 'Software', 'Gravit', 'primary', 'https://www.designer.io/');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Skills`
--
ALTER TABLE `Skills`
  ADD KEY `ID` (`ID`,`Category`,`Name`,`Experience`,`URL`(255));
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
