let canvas = document.createElement("canvas");
let ctx = canvas.getContext("2d");
setup();
let player = {
  "x": 320,
  "y": 220,
  "width": 30,
  "height": 30
};

let keysDown = {};

addEventListener("keydown", function (e) {
  keysDown[e.keyCode] = true;
}, false);

addEventListener("keyup", function (e) {
  delete keysDown[e.keyCode];
}, false);

let update = function (modifier) {
  let pressUp = (38 in keysDown || 87 in keysDown);
  let pressDown = (40 in keysDown || 83 in keysDown);
  let pressLeft = (37 in keysDown || 65 in keysDown);
  let pressRight = (39 in keysDown || 68 in keysDown);
  if (pressUp) {
    player.y--;
  }
  if (pressDown) {
    player.y++;
  }
  if (pressLeft) {
    player.x--;
  }
  if (pressRight) {
    player.x++;
  }
};

let reset = function () {
  player.x = 320;
  player.y = 220;
};

let render = function () {
  ctx.fillStyle = "rgb(120,140,160)";
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  drawPlayer();
};

let main = function () {
  let now = Date.now();
  let delta = now - then;

  update(delta / 1000);
  render();
  then = now;

  requestAnimationFrame(main);
};

let w = window;

requestAnimationFrame = w.requestAnimationFrame ||
  w.webkitRequestAnimationFrame ||
  w.mozRequestAnimationFrame;

// Let's play this game!
let then = Date.now();
reset();
main();


function setup() {
  document.body.style.margin = 0;
  setupCanvas();
}

function setupCanvas() {
  canvas.width = 700;
  canvas.height = 500;
  canvas.style.backgroundColor = "rgb(120,140,160)";
  canvas.setAttribute("id", "canvas");
  document.body.appendChild(canvas);
}

function drawPlayer() {
  ctx.fillStyle = "rgb(80,100,120)";
  ctx.fillRect(player.x, player.y, player.width, player.height);
}