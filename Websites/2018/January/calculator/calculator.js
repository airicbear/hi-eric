let result = document.getElementById('RESULT');
let number1 = "0";
let number2 = "";
let operator = "";

let clearResult = function () {
  number1 = "0";
  number2 = "";
  operator = "";
  result.innerHTML = number1;
}

let pushNumber = function (num) {
  if (operator === "") {
    if (number1 === "0") number1 = num;
    else number1 += num.toString();
    result.innerHTML = number1;
  } else {
    number2 += num;
    result.innerHTML = number2;
  }
}

let pushOperator = function (op) {
  operator += op;
}

let equals = function () {
  if (number1 !== "0" && operator !== "" && number2 !== "") {
    let answer = eval(number1 + operator + number2);
    result.innerHTML = answer;
    number1 = answer;
    number2 = "";
    operator = "";
  }
}