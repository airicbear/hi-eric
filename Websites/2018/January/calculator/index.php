<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Calculator | Eric's Website</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <link rel="shortcut icon" href="/images/cat-icon.ico" type="image/x-icon">
  <style>
    .btn-calc {
      width: 3.75rem;
    }
  </style>
</head>

<body>
  <br>
  <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
  <div class="container text-center" style="width: 25rem;">
    <div class="card">
      <div class="card-header">
        <h1 class="display-3">Calculator</h1>
      </div>
      <div class="card-body">
        <div class="row p-1">
          <div class="col-3">
            <button class="btn btn-secondary p-4 btn-calc" onclick="clearResult()">C</button>
          </div>
          <div class="col-9 btn btn-secondary">
            <p id="RESULT" class="mb-0 text-right" style="font-size: 42px;">0</p>
          </div>
        </div>
        <?php
          function echoNumRow($b1, $b2, $b3, $b4) {
            echo "
          <div class='row p-1'>
            <div class='col-3'>
              <button class='btn btn-secondary p-4 btn-calc' onclick='pushNumber(\"" . $b1 . "\")'>" . $b1 . "</button>
            </div>
            <div class='col-3'>
              <button class='btn btn-secondary p-4 btn-calc' onclick='pushNumber(\"" . $b2 . "\")'>" . $b2 . "</button>
            </div>
            <div class='col-3'>
              <button class='btn btn-secondary p-4 btn-calc' onclick='pushNumber(\"" . $b3 . "\")'>" . $b3 . "</button>
            </div>
            <div class='col-3'>
              <button class='btn btn-secondary p-4 btn-calc' onclick='pushOperator(\"" . $b4 . "\")'>" . $b4 . "</button>
            </div>
          </div>
          ";
          }
          echoNumRow(7,8,9,"+");
          echoNumRow(4,5,6,"-");
          echoNumRow(1,2,3,"*");
          echo "
          <div class='row p-1'>
            <div class='col-3'>
              <button class='btn btn-secondary p-4 btn-calc' onclick='pushNumber(0)'>0</button>
            </div>
            <div class='col-3'>
              <button class='btn btn-secondary p-4 btn-calc' onclick='pushNumber(\".\")'>.</button>
            </div>
            <div class='col-3'>
              <button class='btn btn-secondary p-4 btn-calc' onclick='equals()'>=</button>
            </div>
            <div class='col-3'>
              <button class='btn btn-secondary p-4 btn-calc' onclick='pushOperator(\"/\")'>/</button>
            </div>
          </div>
          ";
        ?>
      </div>
    </div>
  </div>
  <script src="calculator.js"></script>
</body>

</html>